Test Driven Development (TDD)
=============================

.. rst-class:: build

- Made popular by a book called Test Driven Development: By Example by Kent Beck in 2002
- TDD is an approach to software development that uses tests as the requirements for a product
- Before writing your first line of implementation, write a minimal test to just satisfy a requirement and watch it fail
- You can now be confident that the functionality you develop for your implementation will meet that requirement
- Once you see a passing test, use that knowledge to refactor your code
- Work on very short iterations

.. nextslide::

.. figure:: /_static/rgr.png
   :align: center

Why use TDD?
============

.. rst-class:: build

- Encourages simple design
- Encourages the developer to understand the specifics of the problem
- Provides confidence to rework and extend
- Reduces technical debt
- Creates a detailed specification in code
- Regression errors are easily captured
- Produces more readable code
- Implicitly provides coverage
- Keeps a developer focused

Any negatives?
==============

.. rst-class:: build

- You might feel like your project initially moves slower. This will pay off as you are able to move faster later on
- For code that changes often, you may never feel the positives of fast progress later on
- Testing can mean performance sacrifices in your code

Testing at Bank of America Merrill Lynch
========================================

.. figure:: /_static/test_dashboard.jpg
   :align: center

Live demo
=========

Some requirements

#. I can use the anagram finder like ``AnagramFinder(my_words_list).find(my_word)``
#. I get an empty list if no anagrams exist for that word
#. 'tels' is an anagram of 'lets'
#. I can find anagrams for any words
#. A word is not an anagram of itself

.. nextslide::

Extra requirements

#. A word is an anagram of itself if I pass ``self_anagram=True`` into ``.find``
#. Anagrams are case insensitive of my_words_list and my_word
#. I can pass a newline separated word file into the constructor instead

.. nextslide::

Some code to get us started:

.. code-block:: python

    from unittest import unittest

    class AnagramFinder:
        def __init__(self, words):
            pass

        def find(self, input):
            pass

    class TestAnagramFinder(TestCase):
        pass

    if __name__ == '__main__':
        unittest.main()
